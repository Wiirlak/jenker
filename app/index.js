const express = require('express');
const morgan = require('morgan');

const app = express();
app.use(morgan('dev'));

const port = process.env.PORT || 3001;

app.get('/', (req, res) => res.status(200).send('Api working'));

app.use('/api/math', require('./routes/math.router'));
app.use('/api/convert', require('./routes/convert.router'));
app.use('/api/function', require('./routes/function.router'));


app.listen(port, () => console.log('Listening on port ' + port + '...'));