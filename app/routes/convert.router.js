'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const ConvertController = require('../controllers').ConvertController;


const router = express.Router();
router.use(bodyParser.json());


router.get('/', async (req, res, next) => {
    res.status(200).send("convert");
});

router.post('/c2k', async (req, res, next) => {

    if (!req.body.a) {
        return res.status(400).end();
    }
    const result = await ConvertController.celsiusToKelvin(req.body.a)
    res.status(200).json({"result":result})
});

router.post('/k2c', async (req, res, next) => {

    if (!req.body.a) {
        return res.status(400).end();
    }
    const result = await ConvertController.kelvinToCelsius(req.body.a)
    res.status(200).json({"result":result})
});

router.post('/p2c', async (req, res, next) => {

    if (!req.body.a) {
        return res.status(400).end();
    }
    const result = await ConvertController.inchToCm(req.body.a)
    res.status(200).json({"result":result})
});

router.post('/c2p', async (req, res, next) => {

    if (!req.body.a) {
        return res.status(400).end();
    }
    const result = await ConvertController.cmToInch(req.body.a)
    res.status(200).json({"result":result})
});


module.exports = router;