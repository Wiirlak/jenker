'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const FunctionController = require('../controllers').FunctionController;

const router = express.Router();
router.use(bodyParser.json());

router.get('/', async (req, res, next) => {
    res.status(200).send("function");
});

router.post('/max', async (req, res, next) => {
    if (!req.body.a) {
        return res.status(400).end();
    }
    const result = await FunctionController.max(req.body.a)
    res.status(200).json({"result":result})
});

router.post('/min', async (req, res, next) => {

    if (!req.body.a) {
        return res.status(400).end();
    }
    const result = await FunctionController.min(req.body.a)
    res.status(200).json({"result":result})
});



module.exports = router;