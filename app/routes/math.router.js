'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const MathController = require('../controllers').MathController;

const router = express.Router();
router.use(bodyParser.json());

router.get('/', async (req, res, next) => {
    res.status(200).send("math");
});


router.post('/add', async (req, res, next) => {

    if (!req.body.a || !req.body.b) {
        return res.status(400).end();
    }
    const result = await MathController.add(req.body.a, req.body.b)
    res.status(200).json({"result":result})
});

router.post('/minus', async (req, res, next) => {

    if (!req.body.a || !req.body.b) {
        return res.status(400).end();
    }
    const result = await MathController.minus(req.body.a, req.body.b)
    res.status(200).json({"result":result})
});

router.post('/times', async (req, res, next) => {

    if (!req.body.a || !req.body.b) {
        return res.status(400).end();
    }
    const result = await MathController.times(req.body.a, req.body.b)
    res.status(200).json({"result":result})
});

router.post('/divide', async (req, res, next) => {

    if (!req.body.a || !req.body.b || req.body.b <= 0) {
        return res.status(400).end();
    }
    const result = await MathController.divide(req.body.a, req.body.b)
    res.status(200).json({"result":result})
});

router.post('/fib/next', async (req, res, next) => {

    if (!req.body.a) {
        return res.status(400).end();
    }
    const result = await MathController.fiboNext(req.body.a)
    res.status(200).json({"result":result})
});

router.post('/fib', async (req, res, next) => {

    if (!req.body.a) {
        return res.status(400).end();
    }
    const result = await MathController.fib(req.body.a)
    res.status(200).json({"result":result})
});


module.exports = router;