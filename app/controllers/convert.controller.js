'use strict';

class ConvertController {
    celsiusToKelvin(a){
        return a + 273.15;
    }

    kelvinToCelsius(a){
        return a - 273.15;
    }

    inchToCm(a){
        return a * 2.54;
    }

    cmToInch(a){
        return a / 2.54;
    }

}

module.exports = new ConvertController();