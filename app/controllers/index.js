'use strict';

module.exports = {
    MathController: require('./math.controller'),
    FunctionController: require('./function.controller'),
    ConvertController: require('./convert.controller')
};