'use strict';

class FunctionController {
    max(array){
        return array.reduce(function(a,b) {
            return Math.max(a, b);
          });
    }

    min(array){
        return array.reduce(function(a,b) {
            return Math.min(a, b);
          });
    }

}

module.exports = new FunctionController();