# Jenker
Projet de Déploiement Continue réalisé par [Bastien Nisole](https://gitlab.com/Basti3n) & [Alexis Desjardins](https://gitlab.com/Wiirlak) (4AL1)

## Infrastrucutre mise en place:
 * [**Gitlab CI/CD**](https://gitlab.com) : intégration continue et déploiement continue (Docker)
    * Lien des sources : [Jenker repository](https://gitlab.com/Wiirlak/jenker)
    * liens vers les pipelines : [Pipelines Jenker](https://gitlab.com/Wiirlak/jenker/pipelines)
* [**Mochajs**](https://mochajs.org/) : Test unitaire des fonctions de l'API
* [**SonarQube**](https://www.sonarqube.org/) : Inspecteur de qualité de code
    * Interface d'accès : [SonarCloud - Jenker project](https://sonarcloud.io/dashboard?id=Wiirlak_jenker)
*  [**Heroku**](https://www.heroku.com) : Déploiement en ligne (staging & production) de l'API
    * URL de staging : https://wiirlak-ci-staging.herokuapp.com/
    * URL de production : https://wiirlak-ci.herokuapp.com/
* [**Postman**](https://www.postman.com/) : Test des routes de l'API 
    * Accès à l'interface : [**Lien vers la collection**](https://gold-eclipse-6676.postman.co/collections/6824061-f35d4bb1-4384-42eb-a61e-4b7f9c0daafd?version=latest&workspace=bc8c8d6e-c415-4431-8f46-7df2fc78686a)