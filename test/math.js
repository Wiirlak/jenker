var expect = require("chai").expect;
var math = require("../app/controllers").MathController;

describe('Math functions', function() {
	describe('#add(a,b)', function() {
		it('should return the sum of 18 and 25 (43)', function() {
            const value = math.add(18,25);
			expect(value).to.equal(43);
		});
		it('should return the sum of -2 and 4 (2)', function() {
            const value = math.add(-2,4);
			expect(value).to.equal(2);
		});
		it('should return the sum of 7 and 4 (11)', function() {
            const value = math.add(7,4);
			expect(value).to.equal(11);
		});
    });
    
	describe('#minus(a,b)', function() {
		it('should return the substraction of 18 by 25 (43)', function() {
            const value = math.minus(18,25);
			expect(value).to.equal(-7);
		});
		it('should return the substraction of -2 by 4 (2)', function() {
            const value = math.minus(-2,4);
			expect(value).to.equal(-6);
		});
		it('should return the substraction of 7 by 4 (11)', function() {
            const value = math.minus(7,4);
			expect(value).to.equal(3);
		});
	});
    
	describe('#times(a,b)', function() {
		it('should return the product of 18 by 25 (43)', function() {
            const value = math.times(18,25);
			expect(value).to.equal(450);
		});
		it('should return the product of -2 by 4 (2)', function() {
            const value = math.times(-2,4);
			expect(value).to.equal(-8);
		});
		it('should return the product of 7 by 4 (11)', function() {
            const value = math.times(7,4);
			expect(value).to.equal(28);
		});
	});
    
	describe('#divide(a,b)', function() {
		it('should return the substraction of 18 by 25 (43)', function() {
            const value = math.divide(18,25);
			expect(value).to.equal(0.72);
		});
		it('should return the substraction of -2 by 4 (2)', function() {
            const value = math.divide(-2,4);
			expect(value).to.equal(-0.5);
		});
		it('should return the substraction of 7 by 4 (11)', function() {
            const value = math.divide(7,4);
			expect(value).to.equal(1.75);
		});
	});
    
	describe('#fiboNext(num)', function() {
		it('should return the 11th number in fiboNext', function() {
            const value = math.fiboNext(10);
			expect(value).to.equal(89);
		});
		it('should return the 14th number in fiboNext', function() {
            const value = math.fiboNext(13);
			expect(value).to.equal(377);
		});
		it('should return the 4th number in fiboNext', function() {
            const value = math.fiboNext(3);
			expect(value).to.equal(3);
		});
	});
    
	describe('#fib(n)', function() {
		it('should return list of the x numbers until 5', function() {
            const value = math.fib(5);
			expect(value).to.deep.equal([0,1,1,2,3,5]);
		});
	});
});