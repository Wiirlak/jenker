var expect = require("chai").expect;
var func = require("../app/controllers").FunctionController;

describe('Function functions', function() {
	describe('#max(array)', function() {
		it('should return the max value of the given array (898)', function() {
            const value = func.max([2,4,5,898,4]);
			expect(value).to.equal(898);
		});
		it('should return the max value of the given array (74)', function() {
            const value = func.max([74,5,-2,15,69]);
			expect(value).to.equal(74);
		});
		it('should return the max value of the given array (7)', function() {
            const value = func.max([7,4]);
			expect(value).to.equal(7);
		});
    });
	describe('#min(array)', function() {
		it('should return the min value of the given array (2)', function() {
            const value = func.min([2,4,5,898,4]);
			expect(value).to.equal(2);
		});
		it('should return the min value of the given array (-2)', function() {
            const value = func.min([74,5,-2,15,69]);
			expect(value).to.equal(-2);
		});
		it('should return the min value of the given array (4)', function() {
            const value = func.min([7,4]);
			expect(value).to.equal(4);
		});
    });
});