var expect = require("chai").expect;
var convert = require("../app/controllers").ConvertController;

describe('Function functions', function() {
	describe('#celsiusToKelvin(array)', function() {
		it('should return the converted value from C° to Kelvin (273.15)', function() {
			const value = convert.celsiusToKelvin(0);
			expect(value).to.equal(273.15);
		});
		it('should return the converted value from C° to Kelvin (300.15)', function() {
			const value = convert.celsiusToKelvin(27);
			expect(value).to.equal(300.15);
		});
		it('should return the converted value from C° to Kelvin (448.15)', function() {
			const value = convert.celsiusToKelvin(175);
			expect(value).to.equal(448.15);
		});
    });
	describe('#kelvinToCelsius(array)', function() {
		it('should return the converted value from Kelvin to C° (0)', function() {
			const value = convert.kelvinToCelsius(273.15);
			expect(value).to.equal(0);
		});
		it('should return the converted value from Kelvin to C° (27)', function() {
			const value = convert.kelvinToCelsius(300.15);
			expect(value).to.equal(27);
		});
		it('should return the converted value from Kelvin to C° (175)', function() {
			const value = convert.kelvinToCelsius(448.15);
			expect(value).to.equal(175);
		});
    });
	describe('#inchToCm(array)', function() {
		it('should return the converted value from inch to centimeter (1)', function() {
			const value = convert.inchToCm(1);
			expect(value).to.equal(2.54);
		});
		it('should return the converted value from inch to centimeter (190.5)', function() {
			const value = convert.inchToCm(75);
			expect(value).to.equal(190.5);
		});
		it('should return the converted value from inch to centimeter (2484.12)', function() {
			const value = convert.inchToCm(978);
			expect(value).to.equal(2484.12);
		});
    });
	describe('#cmToInch(array)', function() {
		it('should return the converted value from centimeter to inch (2.54)', function() {
			const value = convert.cmToInch(2.54);
			expect(value).to.equal(1);
		});
		it('should return the converted value from centimeter to inch (75)', function() {
			const value = convert.cmToInch(190.5);
			expect(value).to.equal(75);
		});
		it('should return the converted value from centimeter to inch (978)', function() {
			const value = convert.cmToInch(2484.12);
			expect(value).to.equal(978);
		});
    });
});